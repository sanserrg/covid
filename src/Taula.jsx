import React, { useEffect, useState } from "react";
import { Table, Col, Row } from "reactstrap";
import styled from 'styled-components';

const ClicableTh = styled.th`
    text-align: center; 
    cursor: pointer;
    :hover {
        text-decoration: underline;
        color: #97A2FF;
    }
`;

const AlignId = styled.td`
    text-align: center;
    height: 30px;
`;

export default (props) => {

    const filas = props.datos.map((el, i) => (
        <tr key={i}>
            <AlignId>{el.data_ini.substring(0, 10)}</AlignId>
            <AlignId>{el.data_fi.substring(0, 10)}</AlignId>
            <AlignId>{el.ingressos_total}</AlignId>
            <AlignId>{el.casos_confirmat}</AlignId>
        </tr>
    ));

    return (
        <>
            <Table dark style={{ width: "578px", left: '0', position: 'absolute', margin: '-5px'}}>
                <thead>
                    <tr>
                        <ClicableTh>Data Inicial</ClicableTh>
                        <ClicableTh>Data Fin</ClicableTh>
                        <ClicableTh>Ingresos</ClicableTh>
                        <ClicableTh>Confirmados</ClicableTh>
                    </tr>
                </thead>
                <tbody>{filas}</tbody>
                <tfoot></tfoot>
            </Table>
        </>
    );
};
