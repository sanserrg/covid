import React from 'react';
import { useState, useEffect } from "react";
import {
    Col,
    Container,
    Row,
    Dropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap';
import Taula from './Taula';
import Grafico from './Grafico';
import comarcas from './ListaComarcas';

const TriaComarca = () => {

    const [dropdownOpen, setDropdownOpen] = useState(false);
    const toggle = () => setDropdownOpen(prevState => !prevState);
    const [datos, setDatos] = useState([]);
    const [comarca, setComarca] = useState(13);
    const [nom, setNom] = useState('BARCELONÉS')
    const toggleComarca = (codi, nom) => {setComarca((codi)); setNom((nom))};


    useEffect(() => {
        fetch(`https://analisi.transparenciacatalunya.cat/resource/jvut-jxu8.json?codi=${comarca}&residencia=No`)
            .then((response) => response.json())
            .then((data) => {
                setDatos(data.sort((a, b) => {
                    return a.data_ini > b.data_ini ? 1 : -1;
                }).filter((el, index) => index % 7 === 0));
            })
            .catch((error) => console.log(error));
    }, [comarca]);

    const dropDownComarcas = comarcas.map(el => {
        return <DropdownItem key={el.codi} onClick={() => toggleComarca(el.codi, el.nom)}>{el.nom}</DropdownItem>;
    });

    return (
        <Container fluid>
            <Row>
                <Dropdown direction="right" isOpen={dropdownOpen} toggle={toggle}>
                    <h5>{nom}</h5>
                    <DropdownToggle caret>
                        Escoge Comarca
                    </DropdownToggle>
                    <DropdownMenu>
                        {dropDownComarcas}
                    </DropdownMenu>
                </Dropdown>
            </Row>
            <br />
            <Row>
                <Col xs={6}>
                    <Taula datos={datos} />
                </Col>
                <Col xs={6}>
                    <Grafico datos={datos} />
                </Col>
            </Row>
        </Container>
    );
}

export default TriaComarca;


