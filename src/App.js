
import Barcelones from './Barcelones';
import TriaComarca from './TriaComarca';
import Home from './Home';
import { BrowserRouter, Route, Switch, NavLink } from 'react-router-dom';
import { Container } from 'reactstrap';

export default () => {

  return(
    <BrowserRouter>
      <Container>
        <ul className="nav nav-tabs">
        <li className="nav-item">
            <NavLink className="nav-link" to="/home">
            Home
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/barcelones">
            Barcelonés
            </NavLink>
          </li>
          
          <li className="nav-item">
            <NavLink className="nav-link" to="/triaComarca">
            Escoge Comarca
            </NavLink>
          </li>
        </ul>

        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/home" component={Home} />
          <Route path="/barcelones" component={Barcelones} />
          <Route path="/triaComarca" component={TriaComarca} />
        </Switch>
      </Container>
    </BrowserRouter>
    
  );
}
