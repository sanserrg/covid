
import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'reactstrap';
import Taula from './Taula.jsx';
import Grafico from './Grafico';


export default () => {

    const [dades, setDades] = useState([]);

    useEffect(() => {
        fetch("https://analisi.transparenciacatalunya.cat/resource/jvut-jxu8.json?codi=13&residencia=No")
            .then(response => response.json())
            .then(data => setDades(data))
            .catch(err => console.log(err));
    }, [])

    const datos = dades.sort((a, b) => a.data_ini > b.data_ini ? 1 : -1).filter((el, idx) => idx % 7 === 0)

    return (
        <>
            <Container>
                <h1>Covid-19</h1><h4>|| Datos Barcelonés</h4>
                <br />
                <Row>
                    <Col xs={6}>
                        <Taula datos={datos} />
                    </Col>
                    <Col xs={6}>
                        <Grafico datos={datos} />
                    </Col>
                </Row>
            </Container>
        </>
    );
}

