import React from "react";
import {
    XYPlot,
    XAxis,
    YAxis,
    HorizontalGridLines,
    LineSeries,
    VerticalGridLines,
} from "react-vis";

export default (props) => {

    const grafico = props.datos.map((el, i) => (
        { x: new Date(el.data_ini.substring(0, 10)), y: el.ingressos_total }
    ))
    
    return (
        <>
            <XYPlot xType="time" yDomain={[0, 500]} width={600} height={300}>
                <HorizontalGridLines />
                <VerticalGridLines />
                <XAxis title="Fechas" />
                <YAxis title="Casos confirmados" />
                <LineSeries style={{ fill: "none" }} data={grafico} />
            </XYPlot>
        </>
    );
};
